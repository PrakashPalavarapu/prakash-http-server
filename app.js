const http = require("http");
const fsp = require("fs/promises");
const path = require("path");
const { v4: uuid } = require("uuid");

const server = http
  .createServer(async (req, res) => {
    if (req.method == "GET") {
      try {
        let urlParams = req.url.split("/");
        let pathname = urlParams[1];
        console.log(urlParams);
        let files = { html: "index.html", json: "data.json" };
        if (files[pathname]) {
          let contentType =
            pathname == "html" ? "text/html" : "application/json";
          let content = await fsp.readFile(
            path.join(__dirname, "public", files[pathname]),
            "utf-8"
          );
          res.writeHead(200, { "Content-Type": contentType });
          res.write(content);
          res.end();
        } else if (pathname == "uuid") {
          let uuID = uuid();
          res.writeHead(200, { "content-type": "text/plain" });
          res.write(uuID);
          res.end();
        } else if (pathname == "status") {
          let status = urlParams[urlParams.length - 1];
          if (http.STATUS_CODES[status]) {
            res.writeHead(Number(status), { "content-type": "text/plain" });
            res.write(http.STATUS_CODES[status]);
            res.end();
          } else {
            throw Error;
          }
        } else if (pathname == "delay") {
          let delay = urlParams[urlParams.length - 1];
          setTimeout(() => {
            res.writeHead(200, { "content-type": "text/plain" });
            res.write(http.STATUS_CODES["200"]);
            res.end();
          }, delay * 1000);
        } else if (req.url == "/") {
          res.writeHead(200, { "content-type": "text/plain" });
          res.write("Welcome To my sever");
          res.end();
        } else {
          res.writeHead(200, { "content-type": "text/plain" });
          res.write(http.STATUS_CODES["404"]);
          res.end();
        }
      } catch (err) {
        res.writeHead(200, { "content-type": "text/plain" });
        res.write(http.STATUS_CODES["404"]);
        res.end();
      }
    }
  })
  .listen(3000, () => {
    console.log("started server on port 3000");
  });
